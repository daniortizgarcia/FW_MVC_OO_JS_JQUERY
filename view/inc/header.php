<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ohana dogs</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo VENDOR_PATH ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo VENDOR_PATH ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="<?php echo CSS_PATH ?>clean-blog.min.css" rel="stylesheet">

    <link href="<?php echo CSS_PATH ?>style.css" rel="stylesheet" type="text/css">

    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script type="text/javascript" src="<?php echo JS_VIEW_LOGIN ?>init.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH ?>main.js"></script>

    <link href="<?php echo SITE_PATH ?>view/toastr/build/toastr.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?php echo SITE_PATH ?>view/toastr/toastr.js"></script>

  </head>

  <body>