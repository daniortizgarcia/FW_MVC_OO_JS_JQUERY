$(document).ready(function(){

    var token = localStorage.getItem("id_token");
    if (token) {
        $.post('../../login/typeuser/',{'token':token},function(data){
            if (data != 'false') {
                var type = JSON.parse(data);
                if (type[0].type === 'admin') {
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=dogs&funciton=create_dogs') + '">Perros</a></li>');
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=ubication&funciton=list_ubication') + '">Ubicacion</a></li>');
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=login&funciton=profile_list') + '">Profile</a></li>');
                }
                if (type[0].type === 'user') {
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=dogs&funciton=create_dogs') + '">Perros</a></li>');
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=ubication&funciton=list_ubication') + '">Ubicacion</a></li>');
                    $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=login&funciton=profile_list') + '">Profile</a></li>');
                }
            }else{
                $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=login&funciton=list_login') + '">Iniciar Sesion</a></li>');        
            }
        });
    }else{
        $('#print_menu').before('<li class="nav-item" ><a class="nav-link" href="' + amigable('?module=login&funciton=list_login') + '">Iniciar Sesion</a></li>');
    }

});

/*
UBICATION//ADMIN//USER
<li class="nav-item" >
    <a class="nav-link" id="<?php if($_GET['module'] === 'ubication'){echo 'active_menu_color';}else{echo '';} ?>" href="<?php amigable('?module=ubication&funciton=list_ubication'); ?>">Ubicacion</a>
</li>
INICIAR SESION//NOUSER
<li class="nav-item" >
    <a class="nav-link" id="<?php if($_GET['module'] === 'login'){echo 'active_menu_color';}else{echo '';} ?>" href="<?php amigable('?module=login&funciton=list_login'); ?>">Iniciar Sesion</a>
</li>
DOGS//ADMIN//USER
<li class="nav-item">
    <a class="nav-link" id="<?php if($_GET['module'] === 'dogs'){echo 'active_menu_color';}else{echo '';} ?>" href="<?php amigable('?module=dogs&funciton=create_dogs'); ?>">Perros</a>
</li>
*/